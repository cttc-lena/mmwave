#!/bin/bash

#
# Copyright (c) 2017 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Authors: Biljana Bojovic <bbojovic@cttc.es>
#

control_c()
{
  echo "exiting"
  exit $?
}

trap control_c SIGINT

if test ! -f ../../../../waf ; then
    echo "please run this program from within the directory `dirname $0`, like this:"
    echo "cd `dirname $0`"
    echo "./`basename $0`"
    exit 1
fi

outputDir=`pwd`/results

set -x
set -o errexit

# need this as otherwise waf won't find the executables
cd ../../../../

# Random number generator seed
RngRuns="1"
numerologies="0 1 2 3 4 5"
# Packet size in bytes
packetSize=1000  

for numerology in ${numerologies} ; do
  for RngRun in ${RngRuns} ; do      
      /usr/bin/time -f '%e %U %S %K %M %x %C' -o "${outputDir}"/time_stats -a \
     ./waf --run cttc-3gpp-channel-simple-ran --command="%s 
     --numerology=${numerology} 
     --RngRun=${RngRun}
     --packetSize=${packetSize}
     --ns3::MmWaveFlexTtiMacScheduler::FixedMcsDl=1"
  done
done

