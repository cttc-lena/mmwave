#!/bin/bash

#
# Copyright (c) 2017 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Authors: Biljana Bojovic <bbojovic@cttc.es>
#

control_c()
{
  echo "exiting"
  exit $?
}

trap control_c SIGINT

if test ! -f ../../../../waf ; then
    echo "please run this program from within the directory `dirname $0`, like this:"
    echo "cd `dirname $0`"
    echo "./`basename $0`"
    exit 1
fi

outputDir=`pwd`/results

set -x
set -o errexit

# need this as otherwise waf won't find the executables
cd ../../../../

# Random number generator seed
RngRuns="1"
################################
# single UE topology parameters
################################
#singleUeTopology=1
#numerologies="0 1 2 3 4 5"
#packetSize=100
#bandwidth=200e6
#frequency=28e9
#udpFullBuffer=0
#fixedMcs=28
#useFixedMcs=1
#cellScanModes="0"
#gNbNumList="1"

########################################################################################
# multiple UE topology parameters, configuration very similar to that of Wigig module
########################################################################################
singleUeTopology=0
numerologies="4"
packetSize=1000
users="1 2 3 4"
bandwidth=400e6
frequency=60e9
udpFullBuffer=1
useFixedMcs=1
fixedMcs=5
gNbNumList="1 2"

#cell scan mode 0 is long term matrix, 1 is beam search
cellScanModes="0 1" 

for gNbNum in ${gNbNumList} ; do
for cellScan in ${cellScanModes} ; do
 for user in ${users} ; do
  for numerology in ${numerologies} ; do
   for RngRun in ${RngRuns} ; do      
      /usr/bin/time -f '%e %U %S %K %M %x %C' -o "${outputDir}"/time_stats -a \
     ./waf --run cttc-3gpp-channel-nums --command="%s 
     --numerology=${numerology} 
     --RngRun=${RngRun}
     --udpPacketSize=${packetSize}
     --udpFullBuffer=${udpFullBuffer}
     --bandwidth=${bandwidth}
     --frequency=${frequency}
     --ueNumPergNb=${user}
     --singleUeTopology=${singleUeTopology}
     --useFixedMcs=${useFixedMcs}
     --fixedMcs=${fixedMcs}     
     --cellScan=${cellScan}
     --gNbNum=${gNbNum}"
   done
  done
 done
done
done

#################################################################
# try different angles for beamsearchbeamforming, default is 10
#################################################################

#numerologies="4"
#packetSize=1000
#users="1 2 3 4"
#singleUeTopology=0
#bandwidth=400e6
#frequency=60e9
#udpFullBuffer=1
#useFixedMcs=0
#fixedMcs=1
#gNbNumList="1 2"
#cellScanModes=1
#beamSearchAngleStepList="10"
#cellScanModes=1
#beamSearchAngleStepList="10 30"
#
#for gNbNum in ${gNbNumList} ; do
#for beamSearchAngleStep in ${beamSearchAngleStepList} ; do
# for cellScan in ${cellScanModes} ; do
#  for user in ${users} ; do
#   for numerology in ${numerologies} ; do
#    for RngRun in ${RngRuns} ; do      
#      /usr/bin/time -f '%e %U %S %K %M %x %C' -o "${outputDir}"/time_stats -a \
#     ./waf --run cttc-3gpp-channel-nums --command="%s 
#     --numerology=${numerology} 
#     --RngRun=${RngRun}
#     --udpPacketSize=${packetSize}
#     --udpFullBuffer=${udpFullBuffer}
#     --bandwidth=${bandwidth}
#     --frequency=${frequency}
#     --ueNumPergNb=${user}
#     --singleUeTopology=${singleUeTopology}
#     --useFixedMcs=${useFixedMcs}
#     --fixedMcs=${fixedMcs}     
#     --cellScan=${cellScan}
#     --beamSearchAngleStep=${beamSearchAngleStep}
#     --gNbNum=${gNbNum}"
#     done
#    done
#   done
#  done
# done
#done

