/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 *   Copyright (c) 2011 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *   Copyright (c) 2015, NYU WIRELESS, Tandon School of Engineering, New York University
 *  
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2 as
 *   published by the Free Software Foundation;
 *  
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *  
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *  
 *   Author: Marco Miozzo <marco.miozzo@cttc.es>
 *           Nicola Baldo  <nbaldo@cttc.es>
 *  
 *   Modified by: Marco Mezzavilla < mezzavilla@nyu.edu>
 *             Sourjya Dutta <sdutta@nyu.edu>
 *             Russell Ford <russell.ford@nyu.edu>
 *            Menglei Zhang <menglei@nyu.edu>
 */



#include <ns3/object-factory.h>
#include <ns3/log.h>
#include <cfloat>
#include <cmath>
#include <ns3/simulator.h>
#include <ns3/attribute-accessor-helper.h>
#include <ns3/double.h>

#include "mmwave-enb-phy.h"
#include "mmwave-ue-phy.h"
#include "mmwave-net-device.h"
#include "mmwave-ue-net-device.h"
#include "mmwave-spectrum-value-helper.h"
#include "mmwave-radio-bearer-tag.h"

#include <ns3/node-list.h>
#include <ns3/node.h>
#include <ns3/pointer.h>

namespace ns3{

  NS_LOG_COMPONENT_DEFINE ("MmWaveEnbPhy");

  NS_OBJECT_ENSURE_REGISTERED (MmWaveEnbPhy);

  MmWaveEnbPhy::MmWaveEnbPhy ()
  {
    NS_LOG_FUNCTION (this);
    NS_FATAL_ERROR ("This constructor should not be called");
  }

  MmWaveEnbPhy::MmWaveEnbPhy (Ptr<MmWaveSpectrumPhy> dlPhy, Ptr<MmWaveSpectrumPhy> ulPhy)
  :MmWavePhy (dlPhy, ulPhy),
   m_prevVarTti (0),
   m_prevVarTtiDir (VarTtiAllocInfo::NA),
   m_currSymStart (0)
  {
    m_enbCphySapProvider = new MemberLteEnbCphySapProvider<MmWaveEnbPhy> (this);

    Simulator::ScheduleNow (&MmWaveEnbPhy::StartSlot, this);
  }

  MmWaveEnbPhy::~MmWaveEnbPhy ()
  {

  }

  TypeId
  MmWaveEnbPhy::GetTypeId (void)
  {
    static TypeId tid = TypeId ("ns3::MmWaveEnbPhy")
        .SetParent<MmWavePhy> ()
        .AddConstructor<MmWaveEnbPhy> ()
        .AddAttribute ("TxPower",
                       "Transmission power in dBm",
                       DoubleValue (30.0),
                       MakeDoubleAccessor (&MmWaveEnbPhy::SetTxPower,
                                           &MmWaveEnbPhy::GetTxPower),
                                           MakeDoubleChecker<double> ())
        .AddAttribute ("NoiseFigure",
                       "Loss (dB) in the Signal-to-Noise-Ratio due to non-idealities in the receiver."
                       " According to Wikipedia (http://en.wikipedia.org/wiki/Noise_figure), this is "
                       "\"the difference in decibels (dB) between"
                       " the noise output of the actual receiver to the noise output of an "
                       " ideal receiver with the same overall gain and bandwidth when the receivers "
                       " are connected to sources at the standard noise temperature T0.\" "
                       "In this model, we consider T0 = 290K.",
                       DoubleValue (5.0),
                       MakeDoubleAccessor (&MmWavePhy::SetNoiseFigure,
                                           &MmWavePhy::GetNoiseFigure),
                       MakeDoubleChecker<double> ())
       .AddAttribute ("DlSpectrumPhy",
                      "The downlink MmWaveSpectrumPhy associated to this MmWavePhy",
                      TypeId::ATTR_GET,
                      PointerValue (),
                      MakePointerAccessor (&MmWaveEnbPhy::GetDlSpectrumPhy),
                      MakePointerChecker <MmWaveSpectrumPhy> ())
       .AddAttribute ("UlSpectrumPhy",
                      "The uplink MmWaveSpectrumPhy associated to this MmWavePhy",
                      TypeId::ATTR_GET,
                      PointerValue (),
                      MakePointerAccessor (&MmWaveEnbPhy::GetUlSpectrumPhy),
                      MakePointerChecker <MmWaveSpectrumPhy> ())
       .AddTraceSource ("UlSinrTrace",
                       "UL SINR statistics.",
                       MakeTraceSourceAccessor (&MmWaveEnbPhy::m_ulSinrTrace),
                       "ns3::UlSinr::TracedCallback")
         ;
    return tid;

  }

  void
  MmWaveEnbPhy::DoInitialize (void)
  {
    NS_LOG_FUNCTION (this);
    Ptr<SpectrumValue> noisePsd = MmWaveSpectrumValueHelper::CreateNoisePowerSpectralDensity (m_phyMacConfig, m_noiseFigure);
    m_downlinkSpectrumPhy->SetNoisePowerSpectralDensity (noisePsd);
    //m_numRbg = m_phyMacConfig->GetNumRb() / m_phyMacConfig->GetNumRbPerRbg();
    //m_ctrlPeriod = NanoSeconds (1000 * m_phyMacConfig->GetCtrlSymbols() * m_phyMacConfig->GetSymbolPeriod());
    //m_dataPeriod = NanoSeconds (1000 * (m_phyMacConfig->GetSymbPerVarTti() - m_phyMacConfig->GetCtrlSymbols()) * m_phyMacConfig->GetSymbolPeriod());

    for (unsigned i = 0; i < m_phyMacConfig->GetL1L2CtrlLatency(); i++)
      { // push elements onto queue for initial scheduling delay
        m_controlMessageQueue.push_back (std::list<Ptr<MmWaveControlMessage> > ());
      }
    //m_slotAllocInfoUpdated = true;

    for (unsigned i = 0; i < m_phyMacConfig->GetBandwidth(); i++)
      {
        m_channelRbs.push_back(i);
      }

    SetSubChannels(m_channelRbs);

    SfnSf sfnSf = SfnSf (m_frameNum, m_subframeNum , 0, 0);;

    for (unsigned i = 0; i < m_phyMacConfig->GetL1L2DataLatency(); i++)
      {
        //m_slotAllocInfo.push_back (SlotAllocInfo(SfnSf (m_frameNum, m_subframeNum , i, 0)));
        //m_slotAllocInfo.insert(std::make_pair<SfnSf, SlotAllocInfo> (SfnSf (m_frameNum, m_subframeNum , i, 0),
                                               //                      ));

        SlotAllocInfo slotAllocInfo = SlotAllocInfo(sfnSf);

        VarTtiAllocInfo dlCtrlVarTti;
        dlCtrlVarTti.m_varTtiType = VarTtiAllocInfo::CTRL;
        dlCtrlVarTti.m_numCtrlSym = 1;
        dlCtrlVarTti.m_tddMode = VarTtiAllocInfo::DL;
        dlCtrlVarTti.m_dci.m_numSym = 1;
        dlCtrlVarTti.m_dci.m_symStart = 0;
        VarTtiAllocInfo ulCtrlVarTti;
        ulCtrlVarTti.m_varTtiType = VarTtiAllocInfo::CTRL;
        ulCtrlVarTti.m_numCtrlSym = 1;
        ulCtrlVarTti.m_tddMode = VarTtiAllocInfo::UL;
        ulCtrlVarTti.m_varTtiIdx = 0xFF;
        ulCtrlVarTti.m_dci.m_numSym = 1;
        ulCtrlVarTti.m_dci.m_symStart = m_phyMacConfig->GetSymbolsPerSlot()-1;
        slotAllocInfo.m_varTtiAllocInfo.push_back (dlCtrlVarTti);
        slotAllocInfo.m_varTtiAllocInfo.push_back (ulCtrlVarTti);
        SetSlotAllocInfo (slotAllocInfo);
        sfnSf = sfnSf.IncreaseNoOfSlots(m_phyMacConfig->GetSlotsPerSubframe(), m_phyMacConfig->GetSubframesPerFrame());
      }

    MmWavePhy::DoInitialize ();
  }
  void
  MmWaveEnbPhy::DoDispose (void)
  {
    delete m_enbCphySapProvider;
    MmWavePhy::DoDispose();
  }

  void
  MmWaveEnbPhy::SetmmWaveEnbCphySapUser (LteEnbCphySapUser* s)
  {
    NS_LOG_FUNCTION (this);
    m_enbCphySapUser = s;
  }

  LteEnbCphySapProvider*
  MmWaveEnbPhy::GetmmWaveEnbCphySapProvider ()
  {
    NS_LOG_FUNCTION (this);
    return m_enbCphySapProvider;
  }

  void
  MmWaveEnbPhy::SetTxPower (double pow)
  {
    m_txPower = pow;
  }
  double
  MmWaveEnbPhy::GetTxPower () const
  {
    return m_txPower;
  }

  void
  MmWaveEnbPhy::SetNoiseFigure (double nf)
  {
    m_noiseFigure = nf;
  }
  double
  MmWaveEnbPhy::GetNoiseFigure () const
  {
    return m_noiseFigure;
  }

  void
  MmWaveEnbPhy::CalcChannelQualityForUe (std::vector <double> sinr, Ptr<MmWaveSpectrumPhy> ue)
  {

  }

  Ptr<SpectrumValue>
  MmWaveEnbPhy::CreateTxPowerSpectralDensity ()
  {
    Ptr<SpectrumValue> psd =
        MmWaveSpectrumValueHelper::CreateTxPowerSpectralDensity (m_phyMacConfig, m_txPower, m_listOfSubchannels );
    return psd;
  }

  void
  MmWaveEnbPhy::DoSetSubChannels ()
  {

  }

  void
  MmWaveEnbPhy::SetSubChannels (std::vector<int> mask )
  {
    m_listOfSubchannels = mask;
    Ptr<SpectrumValue> txPsd = CreateTxPowerSpectralDensity ();
    NS_ASSERT (txPsd);
    m_downlinkSpectrumPhy->SetTxPowerSpectralDensity (txPsd);
  }

  Ptr<MmWaveSpectrumPhy>
  MmWaveEnbPhy::GetDlSpectrumPhy () const
  {
    return m_downlinkSpectrumPhy;
  }

  Ptr<MmWaveSpectrumPhy>
  MmWaveEnbPhy::GetUlSpectrumPhy () const
  {
    return m_uplinkSpectrumPhy;
  }

  void
  MmWaveEnbPhy::StartSlot (void)
  {
    NS_LOG_FUNCTION (this);
    m_lastSlotStart = Simulator::Now();


    m_currSlotAllocInfo = GetSlotAllocInfo(SfnSf(m_frameNum, m_subframeNum, m_slotNum, 0));
    m_currSfNumVarTtis = m_currSlotAllocInfo.m_varTtiAllocInfo.size ();

    NS_ASSERT ((m_currSlotAllocInfo.m_sfnSf.m_frameNum == m_frameNum) &&
               (m_currSlotAllocInfo.m_sfnSf.m_subframeNum == m_subframeNum) &&
               (m_currSlotAllocInfo.m_sfnSf.m_slotNum == m_slotNum )
               );


    if (m_slotNum == 0)   // send MIB at the beginning of each frame
      {
        LteRrcSap::MasterInformationBlock mib;
        mib.dlBandwidth = (uint8_t)4;
        mib.systemFrameNumber = 1;
        Ptr<MmWaveMibMessage> mibMsg = Create<MmWaveMibMessage> ();
        mibMsg->SetMib(mib);
        if (m_controlMessageQueue.empty())
          {
            std::list<Ptr<MmWaveControlMessage> > l;
            m_controlMessageQueue.push_back (l);
          }
        m_controlMessageQueue.at (0).push_back (mibMsg);
      }
    else if (m_slotNum == 5)  // send SIB at beginning of second half-frame
      {
        Ptr<MmWaveSib1Message> msg = Create<MmWaveSib1Message> ();
        msg->SetSib1 (m_sib1);
        m_controlMessageQueue.at (0).push_back (msg);
      }

    StartVarTti ();
  }

  void
  MmWaveEnbPhy::StartVarTti (void)
  {
    //assume the control signal is omi
    Ptr<AntennaArrayModel> antennaArray = DynamicCast<AntennaArrayModel> (GetDlSpectrumPhy ()->GetRxAntenna());
    antennaArray->ChangeToOmniTx ();

    NS_LOG_FUNCTION (this);

    VarTtiAllocInfo currVarTti;

    /*uint8_t varTtiInd = 0;
 if (m_varTtiNum >= m_currSlotAllocInfo.m_dlVarTtiAllocInfo.size ())
 {
  if (m_currSlotAllocInfo.m_ulVarTtiAllocInfo.size () > 0)
  {
   varTtiInd = m_varTtiNum - m_currSlotAllocInfo.m_dlVarTtiAllocInfo.size ();
   currVarTti = m_currSlotAllocInfo.m_ulVarTtiAllocInfo[varTtiInd];
   m_currSymStart = currVarTti.m_dci.m_symStart;
  }
 }
 else
 {
  if (m_currSlotAllocInfo.m_ulVarTtiAllocInfo.size () > 0)
  {
   varTtiInd = m_varTtiNum;
   currVarTti = m_currSlotAllocInfo.m_dlVarTtiAllocInfo[varTtiInd];
   m_currSymStart = currVarTti.m_dci.m_symStart;
  }
 }*/
    //varTtiInd = m_varTtiNum;
    currVarTti = m_currSlotAllocInfo.m_varTtiAllocInfo[m_varTtiNum];
    m_currSymStart = currVarTti.m_dci.m_symStart;
    SfnSf sfn = SfnSf (m_frameNum, m_subframeNum, m_slotNum, m_varTtiNum);
    std::list <Ptr<MmWaveControlMessage > > dciMsgList;

    Time guardPeriod;
    Time varTtiPeriod;

    if(m_varTtiNum == 0) // DL control var tti
      {
        // get control messages to be transmitted in DL-Control period
        std::list <Ptr<MmWaveControlMessage > > ctrlMsgs = GetControlMessages ();
        //std::list <Ptr<MmWaveControlMessage > >::iterator it = ctrlMsgs.begin ();
        // find all DL/UL DCI elements and create DCI messages to be transmitted in DL control period
        for (unsigned iVarTti = 0; iVarTti < m_currSlotAllocInfo.m_varTtiAllocInfo.size (); iVarTti++)
          {
            if (m_currSlotAllocInfo.m_varTtiAllocInfo[iVarTti].m_varTtiType != VarTtiAllocInfo::CTRL &&
                m_currSlotAllocInfo.m_varTtiAllocInfo[iVarTti].m_tddMode == VarTtiAllocInfo::DL)
              {
                DciInfoElementTdma &dciElem = m_currSlotAllocInfo.m_varTtiAllocInfo[iVarTti].m_dci;
                NS_ASSERT (dciElem.m_format == DciInfoElementTdma::DL);
                if (dciElem.m_tbSize > 0)
                  {
                    Ptr<MmWaveTdmaDciMessage> dciMsg = Create<MmWaveTdmaDciMessage> ();
                    dciMsg->SetDciInfoElement (dciElem);
                    dciMsg->SetSfnSf (sfn);
                    dciMsgList.push_back (dciMsg);
                    ctrlMsgs.push_back (dciMsg);
                  }
              }
          }

        //unsigned ulSlotNum = (m_slotNum + m_phyMacConfig->GetUlSchedDelay ()) % m_phyMacConfig->GetSlotsPerSubframe() ;

        //SfnSf ulSfnSf = sfn.CalculateUplinkSlot
           // (m_phyMacConfig->GetUlSchedDelay(), m_phyMacConfig->GetSlotsPerSubframe(), m_phyMacConfig->GetSubframesPerFrame());

        //SlotAllocInfo slotAllocInfo = GetSlotAllocInfo(ulSfnSf);

        for (unsigned iVarTti = 0; iVarTti < m_currSlotAllocInfo.m_varTtiAllocInfo.size (); iVarTti++)
          {
            if (m_currSlotAllocInfo.m_varTtiAllocInfo[iVarTti].m_varTtiType != VarTtiAllocInfo::CTRL
                && m_currSlotAllocInfo.m_varTtiAllocInfo[iVarTti].m_tddMode == VarTtiAllocInfo::UL)
              {
                DciInfoElementTdma &dciElem = m_currSlotAllocInfo.m_varTtiAllocInfo[iVarTti].m_dci;
                NS_ASSERT (dciElem.m_format == DciInfoElementTdma::UL);
                if (dciElem.m_tbSize > 0)
                  {
                    Ptr<MmWaveTdmaDciMessage> dciMsg = Create<MmWaveTdmaDciMessage> ();
                    dciMsg->SetDciInfoElement (dciElem);
                    dciMsg->SetSfnSf (sfn);
                    //dciMsgList.push_back (dciMsg);
                    ctrlMsgs.push_back (dciMsg);
                  }
              }
          }

        // TX control period
        varTtiPeriod = Seconds (m_phyMacConfig->GetSymbolPeriod () * m_phyMacConfig->GetDlCtrlSymbols());

        NS_LOG_DEBUG ("ENB TXing DL CTRL frame " << m_frameNum <<
                      " subframe " << (unsigned) m_subframeNum <<
                      " slot " << (unsigned) m_slotNum <<
                      " symbols "  << (unsigned) currVarTti.m_dci.m_symStart <<
                      "-" << (unsigned)(currVarTti.m_dci.m_symStart + currVarTti.m_dci.m_numSym - 1)
                      << "\t start " << Simulator::Now() <<
                      " end " << Simulator::Now() + varTtiPeriod - NanoSeconds(1.0));

        SendCtrlChannels(ctrlMsgs, varTtiPeriod - NanoSeconds(1.0)); // -1 ns ensures control ends before data period

      }
    else if (m_varTtiNum == m_currSfNumVarTtis - 1) // UL control var tti
      {
        varTtiPeriod = Seconds (m_phyMacConfig->GetSymbolPeriod () * m_phyMacConfig->GetUlCtrlSymbols());

        NS_LOG_DEBUG ("ENB RXing UL CTRL frame " << m_frameNum <<
                      " subframe " << (unsigned) m_subframeNum <<
                      " slot " << (unsigned) m_slotNum <<
                      " symbols "  << (unsigned) currVarTti.m_dci.m_symStart <<
                      "-" << (unsigned)(currVarTti.m_dci.m_symStart + currVarTti.m_dci.m_numSym-1)
                      << "\t start " << Simulator::Now()
                      << " end " << Simulator::Now() + varTtiPeriod);
      }
    else if (currVarTti.m_tddMode == VarTtiAllocInfo::DL)    // transmit DL var tti
      {
        varTtiPeriod = Seconds (m_phyMacConfig->GetSymbolPeriod() * currVarTti.m_dci.m_numSym);
        NS_ASSERT (currVarTti.m_tddMode == VarTtiAllocInfo::DL);
        //NS_LOG_DEBUG ("Var tti " << m_varTtiNum << " scheduled for Downlink");
        //   if (m_prevVarTtiDir == VarTtiAllocInfo::UL)  // if curr var tti == DL and prev var tti == UL
        //   {
        //    guardPeriod = NanoSeconds (1000.0 * m_phyMacConfig->GetGuardPeriod ());
        //   }
        Ptr<PacketBurst> pktBurst = GetPacketBurst (SfnSf (m_frameNum, m_subframeNum, m_slotNum, currVarTti.m_dci.m_symStart));
        if(pktBurst && pktBurst->GetNPackets() > 0)
          {
            std::list< Ptr<Packet> > pkts = pktBurst->GetPackets ();
            MmWaveMacPduTag macTag;
            pkts.front ()->PeekPacketTag (macTag);
            NS_ASSERT ((macTag.GetSfn().m_slotNum == m_slotNum) && (macTag.GetSfn().m_varTtiNum == currVarTti.m_dci.m_symStart));
          }
        else
          {
            // sometimes the UE will be scheduled when no data is queued
            // in this case, send an empty PDU
            MmWaveMacPduTag tag (SfnSf(m_frameNum, m_subframeNum, m_slotNum, currVarTti.m_dci.m_symStart));
            Ptr<Packet> emptyPdu = Create <Packet> ();
            MmWaveMacPduHeader header;
            MacSubheader subheader (3, 0);  // lcid = 3, size = 0
            header.AddSubheader (subheader);
            emptyPdu->AddHeader (header);
            emptyPdu->AddPacketTag (tag);
            LteRadioBearerTag bearerTag (currVarTti.m_dci.m_rnti, 3, 0);
            emptyPdu->AddPacketTag (bearerTag);
            pktBurst = CreateObject<PacketBurst> ();
            pktBurst->AddPacket (emptyPdu);
          }

        NS_LOG_DEBUG ("ENB TXing DL DATA frame " << m_frameNum <<
                      " subframe " << (unsigned) m_subframeNum <<
                      " symbols "  << (unsigned)currVarTti.m_dci.m_symStart <<
                      "-" << (unsigned)( currVarTti.m_dci.m_symStart + currVarTti.m_dci.m_numSym-1)
                      << "\t start " << Simulator::Now()+NanoSeconds(1.0)
                      << " end " << Simulator::Now() + varTtiPeriod-NanoSeconds (2.0));

        Simulator::Schedule (NanoSeconds(1.0), &MmWaveEnbPhy::SendDataChannels, this, pktBurst, varTtiPeriod - NanoSeconds (2.0), currVarTti);

      }
    else if (currVarTti.m_tddMode == VarTtiAllocInfo::UL)  // receive UL var tti
      {
        varTtiPeriod = Seconds (m_phyMacConfig->GetSymbolPeriod() * currVarTti.m_dci.m_numSym);

        //NS_LOG_DEBUG ("Var tti " << (uint8_t)m_varTtiNum << " scheduled for Uplink");
        m_downlinkSpectrumPhy->AddExpectedTb(currVarTti.m_dci.m_rnti, currVarTti.m_dci.m_ndi, currVarTti.m_dci.m_tbSize,
                                             currVarTti.m_dci.m_mcs, m_channelRbs, currVarTti.m_dci.m_harqProcess, currVarTti.m_dci.m_rv, false,
                                             currVarTti.m_dci.m_symStart, currVarTti.m_dci.m_numSym);

        for (uint8_t i = 0; i < m_deviceMap.size (); i++)
          {
            Ptr<MmWaveUeNetDevice> ueDev = DynamicCast < MmWaveUeNetDevice > (m_deviceMap.at (i));
            uint64_t ueRnti = ueDev->GetPhy ()->GetRnti ();
            //NS_LOG_UNCOND ("Scheduled rnti:"<<rnti <<" ue rnti:"<< ueRnti);
            if (currVarTti.m_rnti == ueRnti)
              {
                //NS_LOG_UNCOND ("Change Beamforming Vector");
                Ptr<AntennaArrayModel> antennaArray = DynamicCast<AntennaArrayModel> (GetDlSpectrumPhy ()->GetRxAntenna());
                antennaArray->ChangeBeamformingVector (m_deviceMap.at (i));
                break;
              }
          }

        NS_LOG_DEBUG ("ENB RXing UL DATA frame " << m_frameNum <<
                      " subframe " << (unsigned) m_subframeNum <<
                      " slot " << (unsigned) m_slotNum <<
                      " symbols "  << (unsigned)currVarTti.m_dci.m_symStart <<
                      "-" << (unsigned)(currVarTti.m_dci.m_symStart + currVarTti.m_dci.m_numSym - 1)
                      << "\t start " << Simulator::Now() <<
                      " end " << Simulator::Now() + varTtiPeriod );
      }

    m_prevVarTtiDir = currVarTti.m_tddMode;

    m_phySapUser->SlotIndication (SfnSf (m_frameNum, m_subframeNum, m_slotNum, m_varTtiNum));  // trigger MAC

    Simulator::Schedule (varTtiPeriod, &MmWaveEnbPhy::EndVarTti, this);

  }

  void
  MmWaveEnbPhy::EndVarTti (void)
  {
    NS_LOG_FUNCTION (this << Simulator::Now ().GetSeconds ());

    Ptr<AntennaArrayModel> antennaArray = DynamicCast<AntennaArrayModel> (GetDlSpectrumPhy ()->GetRxAntenna());
    antennaArray->ChangeToOmniTx ();

    if (m_varTtiNum == m_currSfNumVarTtis - 1)
      {
        m_varTtiNum = 0;
        EndSlot ();

      }
    else
      {
        Time nextVarTtiStart;
        //uint8_t varTtiInd = m_varTtiNum+1;
        /*if (varTtiInd >= m_currSlotAllocInfo.m_varTtiAllocInfo.size ())
  {
   if (m_currSlotAllocInfo.m_varTtiAllocInfo.size () > 0)
   {
    varTtiInd = varTtiInd - m_currSlotAllocInfo.m_varTtiAllocInfo.size ();
    nextVarTtiStart = NanoSeconds (1000.0 * m_phyMacConfig->GetSymbolPeriod () *
                                 m_currSlotAllocInfo.m_ulVarTtiAllocInfo[varTtiInd].m_dci.m_symStart);
   }
  }
  else
  {
   if (m_currSlotAllocInfo.m_varTtiAllocInfo.size () > 0)
   {
    nextVarTtiStart = NanoSeconds (1000.0 * m_phyMacConfig->GetSymbolPeriod () *
                                 m_currSlotAllocInfo.m_varTtiAllocInfo[varTtiInd].m_dci.m_symStart);
   }
  }*/
        m_varTtiNum++;
        nextVarTtiStart = Seconds (m_phyMacConfig->GetSymbolPeriod () *
                                       m_currSlotAllocInfo.m_varTtiAllocInfo[m_varTtiNum].m_dci.m_symStart);

        Simulator::Schedule (nextVarTtiStart + m_lastSlotStart - Simulator::Now(), &MmWaveEnbPhy::StartVarTti, this);
      }
  }

  void
  MmWaveEnbPhy::EndSlot (void)
  {
    NS_LOG_FUNCTION (this << Simulator::Now ().GetSeconds ());

    Time slotStart = m_lastSlotStart + Seconds (m_phyMacConfig->GetSlotPeriod()) - Simulator::Now();

    if (slotStart < Seconds (0))
      NS_FATAL_ERROR ("slotStart value"<<slotStart);

    m_varTtiNum = 0;


    SfnSf sfnf (m_frameNum, m_subframeNum, m_slotNum, m_varTtiNum);

    SfnSf retVal = sfnf.IncreaseNoOfSlots(m_phyMacConfig->GetSlotsPerSubframe(),m_phyMacConfig->GetSubframesPerFrame ());

    m_frameNum = retVal.m_frameNum;
    m_subframeNum = retVal.m_subframeNum;
    m_slotNum = retVal.m_slotNum;

/*    if (m_slotNum == m_phyMacConfig->GetSlotsPerSubframe() - 1)
      {
        m_slotNum = 0;

        if (m_subframeNum == m_phyMacConfig->GetSubframesPerFrame () - 1)
          {
            m_subframeNum = 0;
            m_frameNum++;
          }
        else
          {
            m_subframeNum++;
          }
      }
    else
      {
        m_slotNum = m_slotNum + 1;
      }*/

    Simulator::Schedule (slotStart, &MmWaveEnbPhy::StartSlot, this);
  }

  void
  MmWaveEnbPhy::SendDataChannels (Ptr<PacketBurst> pb, Time varTtiPeriod, VarTtiAllocInfo& varTtiInfo)
  {
    if (varTtiInfo.m_isOmni)
      {
        Ptr<AntennaArrayModel> antennaArray = DynamicCast<AntennaArrayModel> (GetDlSpectrumPhy ()->GetRxAntenna());
        antennaArray->ChangeToOmniTx ();
      }
    else
      { // update beamforming vectors (currently supports 1 user only)
        //std::map<uint16_t, std::vector<unsigned> >::iterator ueRbIt = varTtiInfo.m_ueRbMap.begin();
        //uint16_t rnti = ueRbIt->first;
        for (uint8_t i = 0; i < m_deviceMap.size (); i++)
          {
            Ptr<MmWaveUeNetDevice> ueDev = DynamicCast<MmWaveUeNetDevice> (m_deviceMap.at (i));
            uint64_t ueRnti = ueDev->GetPhy ()->GetRnti ();
            //NS_LOG_UNCOND ("Scheduled rnti:"<<rnti <<" ue rnti:"<< ueRnti);
            if (varTtiInfo.m_dci.m_rnti == ueRnti)
              {
                //NS_LOG_UNCOND ("Change Beamforming Vector");
                Ptr<AntennaArrayModel> antennaArray = DynamicCast<AntennaArrayModel> (GetDlSpectrumPhy ()->GetRxAntenna());
                antennaArray->ChangeBeamformingVector (m_deviceMap.at (i));
                break;
              }

          }
      }

    /*
 if (!varTtiInfo.m_isOmni && !varTtiInfo.m_ueRbMap.empty ())
 {
  Ptr<AntennaArrayModel> antennaArray = DynamicCast<AntennaArrayModel> (GetDlSpectrumPhy ()->GetRxAntenna());
   //set beamforming vector;
   //for ENB, you can choose 64 antenna with 0-15 sectors, or 4 antenna with 0-3 sectors;
   //input is (sector, antenna number)
  antennaArray->SetSector (0,64);
 }
     */

    std::list<Ptr<MmWaveControlMessage> > ctrlMsgs;
    m_downlinkSpectrumPhy->StartTxDataFrames(pb, ctrlMsgs, varTtiPeriod, varTtiInfo.m_varTtiIdx);
  }

  void
  MmWaveEnbPhy::SendCtrlChannels(std::list<Ptr<MmWaveControlMessage> > ctrlMsgs, Time varTtiPeriod)
  {
    /* Send Ctrl messages*/
    NS_LOG_FUNCTION (this<<"Send Ctrl");
    m_downlinkSpectrumPhy->StartTxDlControlFrames (ctrlMsgs, varTtiPeriod);
  }

  bool
  MmWaveEnbPhy::AddUePhy (uint64_t imsi, Ptr<NetDevice> ueDevice)
  {
    NS_LOG_FUNCTION (this<<imsi);
    std::set <uint64_t>::iterator it;
    it = m_ueAttached.find(imsi);

    if (it == m_ueAttached.end ())
      {
        m_ueAttached.insert(imsi);
        m_deviceMap.push_back(ueDevice);
        return (true);
      }
    else
      {
        NS_LOG_ERROR ("Programming error...UE already attached");
        return (false);
      }
  }

  void
  MmWaveEnbPhy::PhyDataPacketReceived (Ptr<Packet> p)
  {
    Simulator::ScheduleWithContext (m_netDevice->GetNode()->GetId(),
                                    MicroSeconds(m_phyMacConfig->GetTbDecodeLatency()),
                                    &MmWaveEnbPhySapUser::ReceivePhyPdu,
                                    m_phySapUser,
                                    p);
    //  m_phySapUser->ReceivePhyPdu(p);
  }

  void
  MmWaveEnbPhy::GenerateDataCqiReport (const SpectrumValue& sinr)
  {
    NS_LOG_FUNCTION (this << sinr);

    Values::const_iterator it;
    MmWaveMacSchedSapProvider::SchedUlCqiInfoReqParameters ulcqi;
    ulcqi.m_ulCqi.m_type = UlCqiInfo::PUSCH;
    int i = 0;
    for (it = sinr.ConstValuesBegin (); it != sinr.ConstValuesEnd (); it++)
      {
        //   double sinrdb = 10 * std::log10 ((*it));
        //       NS_LOG_DEBUG ("ULCQI RB " << i << " value " << sinrdb);
        // convert from double to fixed point notaltion Sxxxxxxxxxxx.xxx
        //   int16_t sinrFp = LteFfConverter::double2fpS11dot3 (sinrdb);
        ulcqi.m_ulCqi.m_sinr.push_back (*it);
        i++;
      }

    // here we use the start symbol index of the var tti in place of the var tti index because the absolute UL var tti index is
    // not known to the scheduler when m_allocationMap gets populated
    ulcqi.m_sfnSf = SfnSf (m_frameNum, m_subframeNum, m_slotNum, m_currSymStart);
    SpectrumValue newSinr = sinr;
    m_ulSinrTrace (0, newSinr, newSinr);
    m_phySapUser->UlCqiReport (ulcqi);
  }


  void
  MmWaveEnbPhy::PhyCtrlMessagesReceived (std::list<Ptr<MmWaveControlMessage> > msgList)
  {
    std::list<Ptr<MmWaveControlMessage> >::iterator ctrlIt = msgList.begin ();

    while (ctrlIt != msgList.end ())
      {
        Ptr<MmWaveControlMessage> msg = (*ctrlIt);

        if (msg->GetMessageType () == MmWaveControlMessage::DL_CQI)
          {
            NS_LOG_INFO ("received CQI");
            m_phySapUser->ReceiveControlMessage (msg);
          }
        else if (msg->GetMessageType () == MmWaveControlMessage::BSR)
          {
            NS_LOG_INFO ("received BSR");
            m_phySapUser->ReceiveControlMessage (msg);
          }
        else if (msg->GetMessageType() == MmWaveControlMessage::RACH_PREAMBLE)
          {
            NS_LOG_INFO ("received RACH_PREAMBLE");
            NS_ASSERT (m_cellId > 0);
            Ptr<MmWaveRachPreambleMessage> rachPreamble = DynamicCast<MmWaveRachPreambleMessage> (msg);
            m_phySapUser->ReceiveRachPreamble (rachPreamble->GetRapId ());
          }
        else if (msg->GetMessageType() == MmWaveControlMessage::DL_HARQ)
          {
            Ptr<MmWaveDlHarqFeedbackMessage> dlharqMsg = DynamicCast<MmWaveDlHarqFeedbackMessage> (msg);
            DlHarqInfo dlharq = dlharqMsg->GetDlHarqFeedback ();
            // check whether the UE is connected
            if (m_ueAttached.find (dlharq.m_rnti) != m_ueAttached.end ())
              {
                m_phySapUser->ReceiveControlMessage (msg);
              }
          }

        ctrlIt++;
      }

  }


  ////////////////////////////////////////////////////////////
  /////////                     sap                 /////////
  ///////////////////////////////////////////////////////////

  void
  MmWaveEnbPhy::DoSetBandwidth (uint8_t ulBandwidth, uint8_t dlBandwidth)
  {
    NS_LOG_FUNCTION (this << (uint32_t) ulBandwidth << (uint32_t) dlBandwidth);
  }

  void
  MmWaveEnbPhy::DoSetEarfcn (uint16_t ulEarfcn, uint16_t dlEarfcn)
  {
    NS_LOG_FUNCTION (this << ulEarfcn << dlEarfcn);
  }


  void
  MmWaveEnbPhy::DoAddUe (uint16_t rnti)
  {
    NS_LOG_FUNCTION (this << rnti);
    bool success = AddUePhy (rnti);
    NS_ASSERT_MSG (success, "AddUePhy() failed");

  }

  bool
  MmWaveEnbPhy::AddUePhy (uint16_t rnti)
  {
    NS_LOG_FUNCTION (this << rnti);
    std::set <uint16_t>::iterator it;
    it = m_ueAttachedRnti.find (rnti);
    if (it == m_ueAttachedRnti.end ())
      {
        m_ueAttachedRnti.insert (rnti);
        return (true);
      }
    else
      {
        NS_LOG_ERROR ("UE already attached");
        return (false);
      }
  }

  void
  MmWaveEnbPhy::DoRemoveUe (uint16_t rnti)
  {
    NS_LOG_FUNCTION (this << rnti);
  }

  void
  MmWaveEnbPhy::DoSetPa (uint16_t rnti, double pa)
  {
    NS_LOG_FUNCTION (this << rnti);
  }

  void
  MmWaveEnbPhy::DoSetTransmissionMode (uint16_t  rnti, uint8_t txMode)
  {
    NS_LOG_FUNCTION (this << rnti << (uint16_t)txMode);
    // UL supports only SISO MODE
  }

  void
  MmWaveEnbPhy::DoSetSrsConfigurationIndex (uint16_t  rnti, uint16_t srcCi)
  {
    NS_LOG_FUNCTION (this);
  }


  void
  MmWaveEnbPhy::DoSetMasterInformationBlock (LteRrcSap::MasterInformationBlock mib)
  {
    NS_LOG_FUNCTION (this);
    //m_mib = mib;
  }


  void
  MmWaveEnbPhy::DoSetSystemInformationBlockType1 (LteRrcSap::SystemInformationBlockType1 sib1)
  {
    NS_LOG_FUNCTION (this);
    m_sib1 = sib1;
  }

  int8_t
  MmWaveEnbPhy::DoGetReferenceSignalPower () const
  {
    NS_LOG_FUNCTION (this);
    return m_txPower;
  }

  void
  MmWaveEnbPhy::SetPhySapUser (MmWaveEnbPhySapUser* ptr)
  {
    m_phySapUser = ptr;
  }

  void
  MmWaveEnbPhy::SetHarqPhyModule (Ptr<MmWaveHarqPhy> harq)
  {
    m_harqPhyModule = harq;
  }

  void
  MmWaveEnbPhy::ReceiveUlHarqFeedback (UlHarqInfo mes)
  {
    NS_LOG_FUNCTION (this);
    // forward to scheduler
    m_phySapUser->UlHarqFeedback (mes);
  }

}
